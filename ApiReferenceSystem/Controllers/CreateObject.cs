﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http;
using System.Web.Script.Serialization;
using ApiReferenceSystem.Models;
using System.Threading.Tasks;

namespace ApiReferenceSystem.Controllers
{
    [RoutePrefix("api/crateObject")]
    public class CrateObjectController : ApiController
    {
        /// <summary>
        /// Возвращает аттибуты для Класса, по которому хотят создать объект
        /// </summary>
        /// <param name="classId">Класс, для которого создаёться объект</param>
        /// <returns>Список аттрибутов объекта, заданного класса</returns>
        [HttpGet, Route("getAttributes")]
        public async Task<IHttpActionResult> GetAttributes(int classId)
        {
            try
            {
                var jsonAttribut = "";
                var returnAttributList = new List<ReturnAttribute>();

                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DbServer"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        connection.Open();
                        cmd.Connection = connection;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "SELECT * FROM Attributes where ClassId = " + classId;

                        var result = await cmd.ExecuteReaderAsync();

                        while (result.Read())
                        {
                            var attribute = new ReturnAttribute
                            {
                                Id = int.Parse(result["Id"].ToString()),
                                TypeId = int.Parse(result["TypeId"].ToString()),
                                Name = result["Name"].ToString()
                            };
                            returnAttributList.Add(attribute);
                        }

                        result.Close();
                        connection.Close();
                    }
                }

                jsonAttribut = new JavaScriptSerializer().Serialize(returnAttributList);

                return Ok(jsonAttribut);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Сохраняет объект для заданного класса с аттрибутами
        /// </summary>
        /// <param name="parameters">json с параметрами нового объекта, в котором:</param>
        /// <param name="parameters">Name - наименование нового бъекта, ClassId - класс, данного объекта, ParentId - id объекта родителя для данного объекта,</param>
        /// <param name="parameters">ChildId - id объекта ребёнка для данного объекта, если создаваемый объект вставляется между двумя уже имеющимися (не обязательно)</param>
        /// <param name="parameters">Attributes - массив из аттрибутов объекта, у которых Id, TypeId - тип атрибута, Data - значение атрибута</param>
        /// <returns></returns>
        [HttpGet, HttpPost, Route("addObject")]
        public async Task<IHttpActionResult> addObject(string parameters)
        {
            try
            {
                dynamic getParameters = Newtonsoft.Json.JsonConvert.DeserializeObject(parameters);
                if (getParameters.ClassId.Value == null || getParameters.ParentId.Value == null || getParameters.Name.Value == "")
                {
                    return BadRequest();
                };

                DataTable attributData = new DataTable();
                attributData.Columns.Add("AttributeId", typeof(int));
                attributData.Columns.Add("ValueDate", typeof(DateTime));
                attributData.Columns.Add("ValueString", typeof(string));
                attributData.Columns.Add("ValueFloat", typeof(double));

                foreach (var attribut in getParameters.Attributes)
                {
                    if (attribut.Id.Value != null)
                    {
                        DataRow attributDataRow = attributData.NewRow();
                        attributDataRow["AttributeId"] = Convert.ToInt32(attribut.Id.Value);
                        attributDataRow["ValueDate"] = (Convert.ToInt32(attribut.TypeId.Value) == 1) ? attribut.Data.Value.ToLocalTime() : DBNull.Value;
                        attributDataRow["ValueString"] = (Convert.ToInt32(attribut.TypeId.Value) == 3) ? attribut.Data.Value.ToString() : DBNull.Value;
                        attributDataRow["ValueFloat"] = (Convert.ToInt32(attribut.TypeId.Value) == 2) ? Convert.ToDecimal(attribut.Data.Value) : DBNull.Value;
                        attributData.Rows.Add(attributDataRow);
                    };
                }

                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DbServer"].ConnectionString;
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (var cmd = connection.CreateCommand())
                    {
                        cmd.CommandText = "dbo.CreateNewObject";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@ClassId", SqlDbType.Int);
                        cmd.Parameters["@ClassId"].Value = Convert.ToInt32(getParameters.ClassId.Value);

                        cmd.Parameters.Add("@Name", SqlDbType.VarChar);
                        cmd.Parameters["@Name"].Value = getParameters.Name.Value.ToString();

                        cmd.Parameters.Add("@ParentId", SqlDbType.Int);
                        cmd.Parameters["@ParentId"].Value = Convert.ToInt32(getParameters.ParentId.Value);

                        cmd.Parameters.Add("@ChildId", SqlDbType.Int);
                        cmd.Parameters["@ChildId"].Value = (getParameters.ChildId.Value != null) ? Convert.ToInt32(getParameters.ChildId.Value) : DBNull.Value;

                        cmd.Parameters.Add("@ObjectAttributes", SqlDbType.Structured);
                        cmd.Parameters["@ObjectAttributes"].TypeName = "ObjectAttributeType";
                        cmd.Parameters["@ObjectAttributes"].Value = attributData;
                        await cmd.ExecuteNonQueryAsync();
                    }
                    connection.Close();
                }

                return Ok();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}