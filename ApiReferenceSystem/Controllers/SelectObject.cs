﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http;
using System.Web.Script.Serialization;
using ApiReferenceSystem.Models;
using System.Threading.Tasks;
using System.Linq;

namespace ApiReferenceSystem.Controllers
{
    [RoutePrefix("api/selectObject")]
    public class SelectObjectController : ApiController
    {
        Dictionary<int, TreeObject> matchKey = new Dictionary<int, TreeObject>();

        private TreeElement recCreateTree(List<TreeObject> objectList, int objectId, TreeElement res)
        {
            var current = objectList.FirstOrDefault(x => x.Id == objectId);
            res.Object = current;
            if (current.ChildId.Count != 0)
            {
                res.Child = new List<TreeElement>();
                foreach (var childId in current.ChildId)
                {
                    var resChild = new TreeElement();
                    res.Child.Add(resChild);
                    resChild = recCreateTree(objectList, childId, resChild);
                }
            }
            return res;
        }

        /// <summary>
        /// Возвращает поддерево для заданного объекта
        /// </summary>
        /// <param name="objectId">Объект, являющийся вершиной поддерева</param>
        /// <returns>Поддерево заданного объекта</returns>
        [HttpGet, Route("getSubtree")]
        public async Task<IHttpActionResult> GetSubtree(int objectId)
        {
            try
            {
                var jsonAttribut = "";
                var returnObjectList = new List<TreeObject>();

                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DbServer"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SelectSubtreeById", connection))
                    {
                        connection.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        var param = new SqlParameter()
                        {
                            ParameterName = "@ObjectId",
                            Direction = ParameterDirection.Input,
                            SqlDbType = SqlDbType.Int,
                            Value = objectId
                        };
                        cmd.Parameters.Add(param);

                        var result = await cmd.ExecuteReaderAsync();

                        while (result.Read())
                        {
                            var attribute = new TreeObject
                            {
                                Id = int.Parse(result["Id"].ToString()),
                                ClassId = int.Parse(result["ClassId"].ToString()),
                                ParentId = int.Parse(result["ParentId"].ToString()),
                                Name = result["Name"].ToString(),
                                ChildId = new List<int>()
                            };
                            returnObjectList.Add(attribute);
                        }

                        result.Close();
                        connection.Close();
                    }
                }

                foreach (var returnObject in returnObjectList)
                {
                    var childObjectList = returnObjectList.Where(x => x.ParentId == returnObject.Id).ToList();
                    foreach (var childObject in childObjectList)
                    {
                        returnObject.ChildId.Add(childObject.Id);
                    }
                };

                var treeElement = new TreeElement();
                var treeElementAll = recCreateTree(returnObjectList, objectId, treeElement);

                jsonAttribut = new JavaScriptSerializer().Serialize(treeElementAll);

                return Ok(jsonAttribut);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Возвращает заданный объект с аттрибутами и значеиниями
        /// </summary>
        /// <param name="objectId">id Объекта, который надо передать</param>
        /// <returns>Объект с аттрибутами и значеиниями</returns>
        [HttpGet, Route("getObject")]
        public async Task<IHttpActionResult> GetObject(int objectId)
        {
            try
            {
                var jsonAttribut = "";
                var returnObjectList = new FullObject();

                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DbServer"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        connection.Open();
                        cmd.Connection = connection;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "SELECT * FROM Objects where Id = " + objectId;

                        var result = await cmd.ExecuteReaderAsync();

                        while (result.Read())
                        {
                            returnObjectList.Id = int.Parse(result["Id"].ToString());
                            returnObjectList.ClassId = int.Parse(result["ClassId"].ToString());
                            returnObjectList.Name = result["Name"].ToString();
                            returnObjectList.Attributes = new System.Collections.ArrayList();
                        }

                        result.Close();
                        connection.Close();
                    }

                    using (SqlCommand cmd = new SqlCommand("SelectObjectById", connection))
                    {
                        connection.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        var param = new SqlParameter()
                        {
                            ParameterName = "@ObjectId",
                            Direction = ParameterDirection.Input,
                            SqlDbType = SqlDbType.Int,
                            Value = objectId
                        };
                        cmd.Parameters.Add(param);

                        var result = await cmd.ExecuteReaderAsync();

                        while (result.Read())
                        {
                            var typeId = int.Parse(result["TypeId"].ToString());
                            var id = int.Parse(result["Id"].ToString());
                            var name = result["Name"].ToString();

                            switch (typeId)
                            {
                                case 1:
                                    var AttributeDate = new ProxyTypeAttribute<DateTime?>()
                                    {
                                        Id = id,
                                        Name = name,
                                        TypeId = typeId
                                    };
                                    if (result["ValueDate"].ToString() != "")
                                    {
                                        AttributeDate.Data = DateTime.Parse(result["ValueDate"].ToString());
                                    }
                                    returnObjectList.Attributes.Add(AttributeDate);
                                    break;
                                case 2:
                                    var AttributeFloat = new ProxyTypeAttribute<float?>()
                                    {
                                        Id = id,
                                        Name = name,
                                        TypeId = typeId
                                    };
                                    if (result["ValueFloat"].ToString() != "")
                                    {
                                        AttributeFloat.Data = float.Parse(result["ValueFloat"].ToString());
                                    }
                                    returnObjectList.Attributes.Add(AttributeFloat);
                                    break;
                                default:
                                    var AttributeString = new ProxyTypeAttribute<string>()
                                    {
                                        Id = id,
                                        Name = name,
                                        TypeId = typeId,
                                        Data = result["ValueString"].ToString()
                                    };
                                    returnObjectList.Attributes.Add(AttributeString);
                                    break;
                            }
                        }

                        result.Close();
                        connection.Close();
                    }
                }

                jsonAttribut = new JavaScriptSerializer().Serialize(returnObjectList);

                return Ok(jsonAttribut);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Возвращает заданный объект с аттрибутами и значеиниями (по примеру из задания, но без Id)
        /// </summary>
        /// <param name="objectId">id Объекта, который надо передать</param>
        /// <returns>Объект с аттрибутами и значеиниями</returns>
        [HttpGet, Route("getObjectNotId")]
        public async Task<IHttpActionResult> GetObjectNotId(int objectId)
        {
            try
            {
                var jsonAttribut = "";
                var asmNameAttribute = new System.Reflection.AssemblyName("newAtibute");
                System.Reflection.Emit.AssemblyBuilder asmBuilderAttribute = AppDomain.CurrentDomain.DefineDynamicAssembly(asmNameAttribute, System.Reflection.Emit.AssemblyBuilderAccess.Run);
                System.Reflection.Emit.ModuleBuilder modBuilderAttribute = asmBuilderAttribute.DefineDynamicModule(asmNameAttribute.Name);
                System.Reflection.Emit.TypeBuilder typeBuilderAttribute = modBuilderAttribute.DefineType("attribute", System.Reflection.TypeAttributes.Class | System.Reflection.TypeAttributes.Public, typeof(object));

                var returnAttributeList = new List<FullAttribute>();
                var returnObjectName = "Default";

                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DbServer"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        connection.Open();
                        cmd.Connection = connection;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "SELECT * FROM Objects where Id = " + objectId;

                        var result = await cmd.ExecuteReaderAsync();

                        while (result.Read())
                        {
                            returnObjectName = result["Name"].ToString();
                        }

                        result.Close();
                        connection.Close();
                    }

                    using (SqlCommand cmd = new SqlCommand("SelectObjectById", connection))
                    {
                        connection.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        var param = new SqlParameter()
                        {
                            ParameterName = "@ObjectId",
                            Direction = ParameterDirection.Input,
                            SqlDbType = SqlDbType.Int,
                            Value = objectId
                        };
                        cmd.Parameters.Add(param);

                        var result = await cmd.ExecuteReaderAsync();

                        while (result.Read())
                        {
                            var typeId = int.Parse(result["TypeId"].ToString());
                            var name = result["Name"].ToString();
                            var newAttribute = new FullAttribute()
                            {
                                Name = name,
                                TypeId = typeId
                            };

                            switch (typeId)
                            {
                                case 1:
                                    if (result["ValueDate"].ToString() != "")
                                    {
                                        newAttribute.ValueDate = DateTime.Parse(result["ValueDate"].ToString());
                                    }
                                    typeBuilderAttribute.DefineField(newAttribute.Name, typeof(DateTime?), System.Reflection.FieldAttributes.Public);
                                    break;
                                case 2:
                                    if (result["ValueFloat"].ToString() != "")
                                    {
                                        newAttribute.ValueFloat = float.Parse(result["ValueFloat"].ToString());
                                    };
                                    typeBuilderAttribute.DefineField(newAttribute.Name, typeof(float?), System.Reflection.FieldAttributes.Public);
                                    break;
                                default:
                                    newAttribute.ValueString = result["ValueString"].ToString();
                                    typeBuilderAttribute.DefineField(newAttribute.Name, typeof(string), System.Reflection.FieldAttributes.Public);
                                    break;
                            };

                            returnAttributeList.Add(newAttribute);
                        }

                        result.Close();
                        connection.Close();
                    }
                }

                Type typeAttribute = typeBuilderAttribute.CreateType();
                var newAttributeList = Activator.CreateInstance(typeAttribute, null);

                foreach(var returnAttribute in returnAttributeList)
                {
                    switch (returnAttribute.TypeId)
                    {
                        case 1:
                            newAttributeList.GetType().GetField(returnAttribute.Name).SetValue(newAttributeList, returnAttribute.ValueDate);
                            break;
                        case 2:
                            newAttributeList.GetType().GetField(returnAttribute.Name).SetValue(newAttributeList, returnAttribute.ValueFloat);
                            break;
                        default:
                            newAttributeList.GetType().GetField(returnAttribute.Name).SetValue(newAttributeList, returnAttribute.ValueString);
                            break;
                    };
                }

                var asmNameObject = new System.Reflection.AssemblyName("NewObject");
                System.Reflection.Emit.AssemblyBuilder asmBuilderObject = AppDomain.CurrentDomain.DefineDynamicAssembly(asmNameObject, System.Reflection.Emit.AssemblyBuilderAccess.Run);
                System.Reflection.Emit.ModuleBuilder modBuilderObject = asmBuilderObject.DefineDynamicModule(asmNameObject.Name);
                System.Reflection.Emit.TypeBuilder typeBuilderObject = modBuilderObject.DefineType("Object", System.Reflection.TypeAttributes.Class | System.Reflection.TypeAttributes.Public, typeof(object));

                typeBuilderObject.DefineField(returnObjectName, typeof(object), System.Reflection.FieldAttributes.Public);

                Type type2 = typeBuilderObject.CreateType();

                var newObject = Activator.CreateInstance(type2, null);
                newObject.GetType().GetField(returnObjectName).SetValue(newObject, newAttributeList);

                jsonAttribut = new JavaScriptSerializer().Serialize(newObject);

                return Ok(jsonAttribut);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Возвращает элеметы поддерева для заданного объекта, которые Класс насос и их масса меньше weight и дата установки меньше date
        /// </summary>
        /// <param name="parameters">параместры: objectId - объект, являющийся вершиной поддерева, аттрибуты: weight - масса объекта, date - дата установки</param>
        /// <returns>Объекты поддерева заданного объекта</returns>
        [HttpPost, Route("getSubtreePump")]
        public async Task<IHttpActionResult> GetSubtreePump(string parameters)
        {
            try
            {
                dynamic getParameters = Newtonsoft.Json.JsonConvert.DeserializeObject(parameters);
                if (getParameters.ObjectId.Value == null || getParameters.Weight.Value == null
                     || getParameters.Date.Value == null)
                {
                    return BadRequest();
                };

                var objectId = Convert.ToInt32(getParameters.ObjectId.Value);
                var classId = 1;
                var dateObject = getParameters.Data.Value.ToLocalTime();
                var weightObject = Convert.ToDouble(getParameters.Weight.Value);

                var jsonAttribut = "";
                var returnObjectList = new List<NameId>();

                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DbServer"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SelectPumpWeightDate", connection))
                    {
                        connection.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        var param = new SqlParameter()
                        {
                            ParameterName = "@ObjectId",
                            Direction = ParameterDirection.Input,
                            SqlDbType = SqlDbType.Int,
                            Value = objectId
                        };
                        cmd.Parameters.Add(param);

                        var param4 = new SqlParameter()
                        {
                            ParameterName = "@ClassId",
                            Direction = ParameterDirection.Input,
                            SqlDbType = SqlDbType.Int,
                            Value = classId
                        };
                        cmd.Parameters.Add(param4);

                        var param2 = new SqlParameter()
                        {
                            ParameterName = "@Weight",
                            Direction = ParameterDirection.Input,
                            SqlDbType = SqlDbType.Float,
                            Value = weightObject
                        };
                        cmd.Parameters.Add(param2);

                        var param3 = new SqlParameter()
                        {
                            ParameterName = "@Date",
                            Direction = ParameterDirection.Input,
                            SqlDbType = SqlDbType.Date,
                            Value = dateObject
                        };
                        cmd.Parameters.Add(param3);

                        var result = await cmd.ExecuteReaderAsync();

                        while (result.Read())
                        {
                            var attribute = new NameId
                            {
                                Id = int.Parse(result["Id"].ToString()),
                                Name = result["Name"].ToString()
                            };
                            returnObjectList.Add(attribute);
                        }

                        result.Close();
                        connection.Close();
                    }
                }

                jsonAttribut = new JavaScriptSerializer().Serialize(returnObjectList);

                return Ok(jsonAttribut);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        /// <summary>
        /// Возвращает элеметы поддерева для заданного объекта, которые удовлетворяют заданным аттрибутами условиям
        /// </summary>
        /// <param name="parameters">параместры: objectId - объект, являющийся вершиной поддерева, ClassId - класс, объекты которого выбираються</param>
        /// <param name="parameters">Attributes - массив из аттрибутов, по которым ведёться поиск, Id - атрибут, по который используется для поиска, TypeId - его тип</param>
        /// <param name="parameters">From, To, LikeStr - параметры для поиска по атрибуту; LikeStr - используется при поиске по подстроке</param>
        /// <param name="parameters">From, To - для поиска по числу и дате, ищет всё что больше From и меньше To </param>
        /// <returns>Объекты поддерева заданного объекта</returns>
        [HttpGet, HttpPost, Route("getSubteeByAttribute")]
        public async Task<IHttpActionResult> addObject(string parameters)
        {
            try
            {

                dynamic getParameters = Newtonsoft.Json.JsonConvert.DeserializeObject(parameters);
                if (getParameters.ClassId.Value == null || getParameters.ObjectId.Value == null)
                {
                    return BadRequest();
                };

                DataTable attributData = new DataTable();
                attributData.Columns.Add("AttributeId", typeof(int));
                attributData.Columns.Add("AttributeTypeId", typeof(int));
                attributData.Columns.Add("From", typeof(string));
                attributData.Columns.Add("To", typeof(string));
                attributData.Columns.Add("LikeStr", typeof(string));

                foreach (var attribut in getParameters.Attributes)
                {
                    if (attribut.Id.Value != null)
                    {
                        DataRow attributDataRow = attributData.NewRow();
                        attributDataRow["AttributeId"] = Convert.ToInt32(attribut.Id.Value);
                        attributDataRow["AttributeTypeId"] = Convert.ToInt32(attribut.TypeId.Value);

                        if (Convert.ToInt32(attribut.TypeId.Value) == 1)
                        {
                            attributDataRow["From"] = (attribut.From != null) ? attribut.From.Value.ToLocalTime().ToString("dd.MM.yyyy") : "";
                            attributDataRow["To"] = (attribut.To != null) ? attribut.To.Value.ToLocalTime().ToString("dd.MM.yyyy") : "";
                        }
                        else if (Convert.ToInt32(attribut.TypeId.Value) == 2)
                        {
                            attributDataRow["From"] = (attribut.From != null) ? attribut.From.Value.ToString() : "";
                            attributDataRow["To"] = (attribut.To != null) ? attribut.To.Value.ToString() : "";
                        }
                        attributDataRow["LikeStr"] = (Convert.ToInt32(attribut.TypeId.Value) == 3) ? attribut.LikeStr.Value.ToString() : "";
                        attributData.Rows.Add(attributDataRow);
                    };
                }

                var jsonAttribut = "";
                var returnObjectList = new List<NameId>();

                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DbServer"].ConnectionString;
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (var cmd = connection.CreateCommand())
                    {
                        cmd.CommandText = "dbo.SelectSubteeByAttribute";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@ObjectId", SqlDbType.Int);
                        cmd.Parameters["@ObjectId"].Value = Convert.ToInt32(getParameters.ObjectId.Value);

                        cmd.Parameters.Add("@ClassId", SqlDbType.Int);
                        cmd.Parameters["@ClassId"].Value = Convert.ToInt32(getParameters.ClassId.Value);

                        cmd.Parameters.Add("@ObjectAttributes", SqlDbType.Structured);
                        cmd.Parameters["@ObjectAttributes"].TypeName = "SearchObjectAttributeType";
                        cmd.Parameters["@ObjectAttributes"].Value = attributData;

                        var result = await cmd.ExecuteReaderAsync();
                        while (result.Read())
                        {
                            var attribute = new NameId
                            {
                                Id = int.Parse(result["Id"].ToString()),
                                Name = result["Name"].ToString()
                            };
                            returnObjectList.Add(attribute);
                        }
                    }
                    connection.Close();
                }

                jsonAttribut = new JavaScriptSerializer().Serialize(returnObjectList);

                return Ok(jsonAttribut);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}