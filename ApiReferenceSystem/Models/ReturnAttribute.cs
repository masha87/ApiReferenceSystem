﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiReferenceSystem.Models
{
    public class ReturnAttribute
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public int TypeId { get; set; }
    }
}