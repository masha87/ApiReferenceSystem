﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiReferenceSystem.Models
{
    public class SearchObjectAttributeType
    {
        public int? AttributeId { get; set; }
        public int? AttributeTypeId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string LikeStr { get; set; }
    }
}