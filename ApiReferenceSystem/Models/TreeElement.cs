﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiReferenceSystem.Models
{
    public class TreeElement
    {
        public TreeObject Object { get; set; }
        public List<TreeElement> Child { get; set; }
    }
}