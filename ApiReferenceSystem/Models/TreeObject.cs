﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiReferenceSystem.Models
{
    public class TreeObject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ClassId { get; set; }
        public int ParentId { get; set; }
        public List<int> ChildId { get; set; }
    }
}