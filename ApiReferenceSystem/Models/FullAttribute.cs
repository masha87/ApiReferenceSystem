﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiReferenceSystem.Models
{
    public class FullAttribute
    {
        public int TypeId { get; set; }
        public string Name { get; set; }
        public DateTime? ValueDate { get; set; }
        public string ValueString { get; set; }
        public float? ValueFloat { get; set; }
    }
}