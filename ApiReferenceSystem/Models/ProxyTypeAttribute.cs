﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiReferenceSystem.Models
{
    ///// <summary>
    ///// Обобщенный прокси класс значения аттрибута
    ///// </summary>
    ///// <typeparam name="T">тип Т</typeparam>
    public class ProxyTypeAttribute<T>
    {
        public ProxyTypeAttribute() { }

        public ProxyTypeAttribute(int id, int typeId, string name, T data)
        {
            Data = data;
            Id = id;
            TypeId = typeId; 
            Name = name;
        }

        /// <summary>
        /// Значение аттрибута
        /// </summary>
        public T Data { get; set; }

        public int Id { get; set; }
        public int TypeId { get; set; }
        public string Name { get; set; }


    }
}