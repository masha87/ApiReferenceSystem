﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiReferenceSystem.Models
{
    public class ObjectAttributeType
    {
        public int? AttributeId { get; set; }
        public DateTime? ValueDate { get; set; }
        public string ValueString { get; set; }
        public double? ValueFloat { get; set; }
    }
}