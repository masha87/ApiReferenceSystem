﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiReferenceSystem.Models
{
    public class FullObject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ClassId { get; set; }
        public System.Collections.ArrayList Attributes { get; set; }
    }
}